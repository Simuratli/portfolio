import React from 'react'
import './sidebar.css'
import Button from '../Button/button'
import Projects from '../../assets/directory.png'
import About from '../../assets/adress.png'
import {connect} from 'react-redux'
import * as actionTypes from '../../store/modal/actions'

const buttons  = [
    
    {
        title:'Projects',
        image:Projects
    },
    {
        title:'About',
        image:About
    }
]


function Sidebar(props) {
    
    function opener(text){
        props.onStartMenuOpenText(text)
        props.onStartMenuOpen()
    }
    

    return (
        <div className='sidebar'>
            {
                buttons.map((item,idx)=>{
                    return <Button onClick={()=>opener(item.title)} key={idx} title={item.title} image = {item.image} />
                })
            }
        </div>
    )
}



const mapDispatchToProps = dispatch => {
    return{
        onStartMenuOpen: () => dispatch({type:actionTypes.OPEN_MODAL}),
        onStartMenuOpenText: (text) => dispatch({type:actionTypes.OPEN_ITEM,name:text})
    }
}

export default connect(null,mapDispatchToProps)(Sidebar)
