import React from 'react'
import {Howl} from 'howler';
import Click from '../../assets/sound/click.mp3'
import './button.css'

function Button(props) {
    var sound = new Howl({
        src: [Click]
    });

    function soundAndModal(){
        sound.play();
    }
    
    return (
        <button className='main_button' onMouseUp={soundAndModal} onClick={props.onClick}>
            <img src={props.image} alt={props.title}/>
            {props.title}
        </button>
    )
}

export default Button
