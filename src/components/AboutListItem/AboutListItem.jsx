import React from 'react'
import './AboutListItem.css'

function AboutListItem(props) {
    return (
        <div className="about_list_item">
           <div className="about_list_img">
               {props.children}
           </div>
           <h3 className="about_list_name">{props.name}</h3>
        </div>
    )
}

export default AboutListItem
