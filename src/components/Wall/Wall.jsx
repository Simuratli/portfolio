import React from 'react'
import { connect } from 'react-redux'
import './wall.css'

// images  
// img 1 https://i.imgyukle.com/2021/03/14/NiWfrc.gif
// img 2 https://i.imgyukle.com/2021/03/14/NieE50.gif
// img 3 https://i.imgyukle.com/2021/03/14/Niex78.gif
// img 4 https://i.imgyukle.com/2021/03/14/Niec2G.gif
// img 5 https://i.imgyukle.com/2021/03/14/Ni3gnp.gif


function Wall(props) {
    return (
        <div style={{ backgroundImage: `url(${!localStorage.getItem('img') ? 'https://i.pinimg.com/originals/b2/2a/a2/b22aa22b2f3f55b6468361158d52e2e7.gif': localStorage.getItem('img')})` }} className='wall'>
            {props.children}
        </div>
    )
}

const mapStateToProps = (state) =>{
        return{
            wallpaper:state.wallReducer.image
        }
}

export default connect(mapStateToProps)(Wall)
