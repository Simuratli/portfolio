import React from 'react'
import Profile from '../../assets/about/me.jpg'
import Cat from '../../assets/about/cat.gif'
import Bat from '../../assets/about/bat.gif'
import './About.css'
import Kiki from '../../assets/about/kiki.gif'
import AboutListItem from '../AboutListItem/AboutListItem'
import Totoro from "../../assets/about/totoro.gif";
import Bitbucket from '../../assets/contact/bitbucket.svg'
import GitHub from '../../assets/contact/github.svg'
import Linkedin from '../../assets/contact/linkedin.svg'
import Twitter from '../../assets/contact/twitter.svg'
import Medium from '../../assets/contact/medium.svg'
import CV from '../../cv/Eljan Simuratli.pdf'

function About() {
    return (
        <div className='about'>
            <img className='bat_img' src={Bat} alt="Bat"/>
            <div className="about_heading">
                <div className="about_image">
                    <img src={Profile} alt="Profile"/>
                </div>
                <div className="about_heading_content">
                <h4>Hello world <img className='about_heading_content_img' src="https://github.com/TheDudeThatCode/TheDudeThatCode/raw/master/Assets/Earth.gif" alt="Earth"/></h4>
                <h1>Simuratli<span className='tag'>.is()</span> <a className='button_resume' download href={CV}>resume</a></h1>
                <h6 className='purple'>Eljan Simuratli</h6>
                <h3>Creative Front-end developer</h3>
                <img width='117px' src={Cat} alt="Cat"/>
                </div>
            </div>
            <br/>
            <br/>
            <div className="about_programming_languages">
                <AboutListItem name='React & Next js'>
                    <i className="devicon-react-original"></i>
                </AboutListItem>
                <AboutListItem name='Redux'>
                    <i className="devicon-redux-original"></i>
                </AboutListItem>
                <AboutListItem name='CSS 3'>
                    <i className="devicon-css3-plain"></i>
                </AboutListItem>
                <AboutListItem name='HTML 5'>
                    <i className="devicon-html5-plain"></i>
                </AboutListItem>    

                <AboutListItem name='Gulp'>
                    <i className="devicon-gulp-plain"></i>
                </AboutListItem>    

                <AboutListItem name='JavaScript'>
                    <i className="devicon-javascript-plain"></i>
                </AboutListItem>    

                <AboutListItem name='Jquery'>
                    <i className="devicon-jquery-plain"></i>
                </AboutListItem> 

                <AboutListItem name='Node js'>
                    <i className="devicon-nodejs-plain"></i>
                </AboutListItem>    

                <AboutListItem name='SASS'>
                    <i className="devicon-sass-original"></i>
                </AboutListItem>          

                <AboutListItem name='TypeScript'>
                    <i className="devicon-typescript-plain"></i>
                </AboutListItem>       
            </div>
            <br/>
           <div className="center">
            <AboutListItem name='And More ...'></AboutListItem> 
           </div>
             <br/>
             <br/>
            <div className="experience_container">
            <div className="about_experiences">
               <div className="expreience-line">
                    <span className="experience-dot"></span>
                    <span className="experience-black-line"></span>
               </div>
               <div className="experience-text">
                   <h1>Web developer</h1>
                   <h4>TVB Group</h4>
                   <ul>
                       <li>Designing the layouts of website</li>
                       <li>Converting PSD to HTML</li>
                       <li>Write back side of project</li>
                   </ul>
               </div>
           </div>
           <div className="about_experiences">
               <div className="expreience-line">
                    <span className="experience-dot"></span>
                    <span className="experience-black-line"></span>
               </div>
               <div className="experience-text">
               <h1>Front end developer</h1>
                   <h4>Pragmatech</h4>
                   <ul>
                       <li>Working closely to the backend team</li>
                       <li>Working on different frontend projects</li>
                       <li>Organization of files.</li>
                   </ul>
               </div>

           </div>
           <div className="about_experiences">
               <div className="expreience-line">
                    <span className="experience-dot"></span>
                    <span className="experience-black-line"></span>
               </div>
               <div className="experience-text">
               <h1>Front end developer and junior designer</h1>
                   <h4>More than 2 years freelance</h4>
                   <ul>
                       <li>Develope different sweet projects</li>
                       <li>Design different project in Figma</li>
                       <li>Organization of files.</li>
                   </ul>
               </div>
               
           </div>
            </div>
            <br/>
            <div className="fun_fact_container">
                    <div className="fun_heading">
                        <h1 className='fun_fact_head'>Fun Facts </h1><img width='100px' src={Kiki} alt="Kiki"/>
                    </div>

                    <div style={{backgroundImage:`url(${Totoro})`}} className="fun_background">
                        <p>Meanwhile...
                            <br/>
                        studying <span className='purple'>Philosophy</span>, <span className="purple">Japanese animation</span> and <span className="purple">video games</span>.</p>
                    </div>
            </div>
            <br/>
            <br/>
            <div className="contact">
                <div className="contact_head">
                    <h1>Contact with me</h1>
                    <img src="https://github.com/TheDudeThatCode/TheDudeThatCode/raw/master/Assets/Handshake.gif" alt=""/>
                </div>
                <div className="contact_buttons">
                    <a target="_blank" rel="noreferrer" href="https://www.linkedin.com/in/elcan-simuratli-36678818a/"><img src={Linkedin} alt=""/></a>
                    <a target="_blank" rel="noreferrer" href="https://github.com/Simuratli"><img src={GitHub} alt=""/></a>
                    <a target="_blank" rel="noreferrer" href="https://twitter.com/simuratli1"><img src={Twitter} alt=""/></a>
                    <a target="_blank" rel="noreferrer" href="https://bitbucket.org/Simuratli/"><img src={Bitbucket} alt=""/></a>
                    <a target="_blank" rel="noreferrer" href="https://medium.com/@simuratli"><img src={Medium} alt=""/></a>
                </div>
            </div>
        </div>
    )
}

export default About
