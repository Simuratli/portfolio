import React from 'react'
import './StartMenu.css'
import {Howl} from 'howler';
import Click from '../../assets/sound/click.mp3'
import Start from '../../assets/start.png'
import Directory from '../../assets/directory.png'
import Int from '../../assets/int.png'
import Music from '../../assets/music.png'
import Sleep from '../../assets/sleep.png'
import Wallpaper from '../../assets/wallpaper.png'
import Adress from '../../assets/adress.png'
import {connect} from 'react-redux'
import * as actionTypes from '../../store/startMenu/actions'
import * as modalActionTypes from '../../store/modal/actions'
import * as sleepActionTypes from '../../store/sleep/actions'

function StartMenu(props) {
    var sound = new Howl({
        src: [Click]
    });

    function openMenu(){
        props.onStartMenuOpen()
    }

    function openModal(params) {
            props.onStartMenuOpenText(params)
            sound.play()
            props.onStartModalOpen()
            props.onStartMenuOpen()
    }       

    function sleep() {
        props.onSleepMode()
        props.onStartMenuOpen()
    }

    return (
        <>
        <div onClick={openMenu} className={props.open ? 'backdrop' : ''}></div>
        <div className='startMenu'>
            <div className={`opener ${props.open && 'open'}`}>
                <div className="opener-text">
                    <p className='opener_P'>Windows98</p>
                </div>
                    <div className="openerMenu">
                        <button onClick={()=>openModal('Projects')} className='startMenu_button'>
                            <img src={Directory} alt="Projects"/>
                            Projects
                        </button>
                        <button onClick={()=>openModal('Music')} className='startMenu_button'>
                            <img src={Music} alt="Music"/>
                            Music
                        </button>
                        <button onClick={()=>openModal('About')} className='startMenu_button'>
                            <img src={Adress} alt="About"/>
                            About
                        </button>
                        <button onClick={()=>openModal('Wallpaper')} className='startMenu_button'>
                            <img src={Wallpaper} alt="wallpaper"/>
                            Wallpaper
                        </button>
                        <div className="line"></div>
                        <button onClick={()=>openModal('Internet')} className='startMenu_button'>
                            <img src={Int} alt="Internet"/>
                            Internet
                        </button>
                        <button onClick={()=>sleep()} className='startMenu_button'>
                            <img src={Sleep} alt="Sleep"/>
                            Sleep
                        </button>
                        
                    </div>
            </div>
            <button onClick={()=>{sound.play(); openMenu()}}>
                <img src={Start} alt="Start"/>
            </button>  
        </div>
        </>
    )
}

const mapStateToProps = state => {
    return {
        open:state.startMenuReducer.startMenu,
    }
}

const mapDispatchToProps = dispatch => {
    return{
        onStartMenuOpen: () => dispatch({type:actionTypes.OPEN_MENU}),
        onStartMenuOpenText: (text) => dispatch({type:modalActionTypes.OPEN_ITEM,name:text}),
        onStartModalOpen: () => dispatch({type:modalActionTypes.OPEN_MODAL}),
        onSleepMode: () => dispatch({type:sleepActionTypes.OPEN_SLEEP}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(StartMenu)
