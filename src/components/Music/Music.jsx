import React,{useState} from 'react'
import {Howl} from 'howler';
import Totoro from '../../assets/music/Path Of The Wind (Totoro OST) - Joe Hisaishi(Piano ver.).mp3'
import Kiki from "../../assets/music/Kiki's Delivery Service - A Town With An Ocean View - Main Theme.mp3"
import Country from "../../assets/music/Country Roads [Whisper of the Heart] (John Denver) With lyrics.mp3"
import Cruel from "../../assets/music/A Cruel Angel s Thesis (Director s Edit Version) - Shiro Sag.m4a"
import Click from '../../assets/sound/click.mp3'
// Albums 
import KikiAlbum from '../../assets/album/kiki.jpg'
import TotoroAlbum from '../../assets/album/totoro.jpg'
import CruelAlbum from '../../assets/album/cruel.jpg'
import CountryAlbum from '../../assets/album/country.jpg'
import Disk from '../../assets/album/disk.png'
// Css
import './Music.css'
//Buttons
import Play from '../../assets/play-circle.svg'
import Pause from '../../assets/pause-circle.svg'


var sound = new Howl({
    src: [Click]
});

var countryRoads = new Howl({
    src: Country,
    loop: true,
});

var cruenAngel = new Howl({
    src: Cruel,
    loop: true,
});

var kiki = new Howl({
    src: Kiki,
    loop: true,
});

var totoro = new Howl({
    src: Totoro,
    loop: true,
});


function Music() {

    
    const [disk, setDisk] = useState({
        img:Disk
    })

    const [play, setPlay] = useState(false)

    const [playIndividual, setPlayIndividual] = useState({
        totoro:false,
        cruel:false,
        country:false,
        kiki:false,
    })

    const [num, setNum] = useState({
        totoro:1,
        cruel:1,
        country:1,
        kiki:1,
    })

    
    function music(param) {
        if(param === 'Totoro'){
            setPlayIndividual({
                totoro:true
            })
            sound.play();
            setDisk({
                img:TotoroAlbum
            })
            setNum((prev)=>({
                ...prev,
                totoro:num.totoro+1
            }))
            if(num.totoro % 2 === 0){
                totoro.stop()
                setPlayIndividual((prev)=>({
                    totoro:false
                }))
            } else{
                totoro.play()
                setPlayIndividual((prev)=>({
                    totoro:true
                }))
            }
            setPlay(true)
            kiki.stop()
            cruenAngel.stop()
            countryRoads.stop()
        }
        if(param === 'Kiki'){
            setPlay(true)
            sound.play();
            setDisk({
                img:KikiAlbum
            })
            setNum((prev)=>({
                ...prev,
                kiki:num.kiki+1
            }))
            if(num.kiki % 2 === 0){
                kiki.stop()
                setPlayIndividual((prev)=>({
                    kiki:false
                }))
            } else{
                kiki.play()
                setPlayIndividual((prev)=>({
                    kiki:true
                }))
            }
            totoro.stop()
            cruenAngel.stop()
            countryRoads.stop()
        }
        if(param === 'Cruel'){
            setPlay(true)
            sound.play();
            setDisk({
                img:CruelAlbum
            })
            setNum((prev)=>({
                ...prev,
                cruel:num.cruel+1
            }))
            if(num.cruel % 2 === 0){
                cruenAngel.stop()
                setPlayIndividual((prev)=>({
                    cruel:false
                }))
            } else{
                cruenAngel.play()
                setPlayIndividual((prev)=>({
                    cruel:true
                }))
            }
            kiki.stop()
            totoro.stop()
            countryRoads.stop()
        }
        if(param === 'Country'){
            setPlay(true)
            sound.play();
            setDisk({
                img:CountryAlbum
            })
            setNum((prev)=>({
                ...prev,
                country:num.cruel+1
            }))
            if(num.country % 2 === 0){
                countryRoads.stop()
                setPlayIndividual((prev)=>({
                    country:false
                }))
            } else{
                countryRoads.play()
                setPlayIndividual((prev)=>({
                    country:true
                }))
            }
            kiki.stop()
            totoro.stop()
            cruenAngel.stop()
        }

    }

    function pause(){
        setPlay(false)
        sound.play();
        kiki.stop()
        totoro.stop()
        cruenAngel.stop()
        countryRoads.stop()
        setDisk({
            img:Disk
        })
        setPlayIndividual((prev)=>({
            ...prev,
            totoro:false,
            cruel:false,
            country:false,
            kiki:false,
        }))
    }

    return (
        <div className='music_container'>
            <div className="music_image">
                <div className="music_disk">
                    <img  className={`diskImage ${play && 'play'}`} src={disk.img} alt="Disk"/>
                    <button  onClick={()=>pause()} className='pause'><img  src={play ? Pause : Play} alt=""/></button>
                </div>
            </div>
            <br/>
            <div className="music_list">
                <div className={`music_list_item ${playIndividual.totoro && 'bgcolor'}`}>
                    <button onClick={()=>music('Totoro')}>
                        <img src={Play} alt="Play"/>
                    </button>
                    <h3 className="music_name">Path Of The Wind (Totoro OST) - Joe Hisaishi</h3>
                </div>
                <div className={`music_list_item ${playIndividual.kiki && 'bgcolor'}`}>
                    <button onClick={()=>music('Kiki')}>
                        <img src={Play} alt="Play"/>
                    </button>
                    <h3 className="music_name">Kiki's Delivery Service - A Town With An Ocean View</h3>
                </div>
                <div className={`music_list_item ${playIndividual.cruel && 'bgcolor'}`}>
                    <button onClick={()=>music('Cruel')}>
                        <img src={Play} alt="Play"/>
                    </button>
                    <h3 className="music_name">A Cruel Angel s Thesis (Director s Edit Version)</h3>
                </div>
                <div className={`music_list_item ${playIndividual.country && 'bgcolor'}`}>
                    <button onClick={()=>music('Country')}>
                        <img src={Play} alt="Play"/>
                    </button>
                    <h3 className="music_name">Country Roads [Whisper of the Heart]</h3>
                </div>
            </div>
        </div>
    )
}

export default Music
