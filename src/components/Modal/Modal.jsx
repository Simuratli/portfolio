import React,{useState} from 'react'
import './Modal.css'
import Folder from '../../assets/folder.png'
import Close from '../../assets/close.png'
import {connect} from 'react-redux'
import * as actionTypes from '../../store/modal/actions'

function Modal(props) {
    const [full, setFull] = useState(false)
    function fullScreen(){
        setFull(!full)
    }
    
    return (
        <div className={`modal ${props.modalOpen && 'showingModal'} ${full && 'fulscreen'}`}>
            <div className="modal_head">
                <h6>{props.title}</h6>
                <div className="modal_buttons">
                    <button onClick={fullScreen}><img src={Folder} alt="Folder"/></button>
                    <button onClick={()=>props.onModalOpen()}><img src={Close} alt="CLOSE"/></button>
                </div>
            </div>

            <div className="content">
                {props.children}
            </div>
        </div>
    )
}


const mapStateToProps = state => {
    return{
        modalOpen:state.modalReducer.modal
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        onModalOpen:()=>{dispatch({type:actionTypes.OPEN_MODAL})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Modal)
