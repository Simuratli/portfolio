import React,{useState} from 'react'
import './Sleep.css'
import {connect} from 'react-redux'
import  * as actions from '../../store/sleep/actions'

function Sleep(props) {
    const [button, setButton] = useState(false)


    function showButton () {
        setButton(true)
    }
    
    function closeSleep(){
        props.onSleepMode()
    }

    return (
        <div onClick={showButton} className={`sleep ${!props.sleep && 'closeSleep'}`}>
            <div id='stars'></div>
            <div id='stars2'></div>
            <div id='stars3'></div>
            <div id='title'>
            <br/>
            <div>
            <div>Click to screen</div>
                <button onClick={closeSleep} className={`sleep_button ${button && 'openbutton'}`}>OPEN</button>
            </div>
            </div>
        </div>
    )
}


const mapStateToProps = state =>{
    return{
        sleep:state.sleepReducer.sleep
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        onSleepMode:()=>{dispatch({type:actions.OPEN_SLEEP})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Sleep)
