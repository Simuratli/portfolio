import React from 'react'
import {connect} from 'react-redux'
import * as actionTypes from '../../store/wallpaper/actions'
import './Walpaper.css'


// images  
// img 1 https://i.imgyukle.com/2021/03/14/NiWfrc.gif
// img 2 https://i.imgyukle.com/2021/03/14/NieE50.gif
// img 3 https://i.imgyukle.com/2021/03/14/Niex78.gif
// img 4 https://i.imgyukle.com/2021/03/14/Niec2G.gif
// img 5 https://i.imgyukle.com/2021/03/14/Ni3gnp.gif

var buttons = [
    {img1:'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/9bc27292880429.5e569ff84e4d0.gif'},
    {img1:'https://www.themasterpicks.com/wp-content/uploads/2020/04/22b22287602523.5dbd29081561d.gif'},
    {img1:'https://i.pinimg.com/originals/f1/63/11/f16311fd0c32786525f471c685bc516e.gif'},
    {img1:'https://steamuserimages-a.akamaihd.net/ugc/906779635937338915/72C9F4F0967EC184AB98089C7F1F364A41E913C2/'},
    {img1:'https://i.pinimg.com/originals/77/ca/a3/77caa32884d735d439ade45ba37feaf2.gif'},
]
    
    



function Wallpaper(props) {

   
    return (
        <div className='wallpapers'>
        <h1>Change your wallpaper</h1>
        <br/>
            <div className="wallapper_buttons">
               
               {buttons.map((item,idx)=>{
                   return (
                    <button key={idx} className='wallapper_button'  onClick={()=>{props.onWallpaperChange(item.img1); }}>
                        <img src={item.img1}  alt='Background' />
                    </button>
                   )
               })}
            </div>
        </div>
    )
}


const mapDispatchToProps = (dispatch) =>{
    return{
        onWallpaperChange:(img)=>{dispatch({type:actionTypes.CHANGE_WALLPAPER,image:img})}
    }
}

export default connect(null,mapDispatchToProps)(Wallpaper)