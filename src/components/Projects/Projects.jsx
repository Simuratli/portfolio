import React,{useEffect,useState} from 'react'
import './Projects.css'
import Skeleton  from '../Skeleton/Skeleton';
import sanityClient from '../../sanity'
import imageUrlBuilder from "@sanity/image-url";
import Totoro from '../../assets/totoro.png'
const builder = imageUrlBuilder(sanityClient);
function urlFor(source) {
  return builder.image(source);
}
function Projects() {

    const [data, setData] = useState([])
    const [loader, setloader] = useState(true)

    useEffect(() => {
        sanityClient.fetch(`*[_type == "projects"]`)
          .then((data) => {
            setData(data)
            setloader(false)
          })
          .catch(console.error);
      }, []);

      let CARD_DATA = data && data.map((item,index)=>{
          return (
              <div key={index}>
                <br/>
                    <div className="projects_card">
                        <div className="projects_card_image">
                            <img src={urlFor(item.image)} alt="portfolio"/>
                        </div>
                        <div className="projects_card_content">
                            <div className="projects_card_text">
                                <h1><span className="purple">{item.name}</span></h1>
                                {item.description}
                            </div>
                            <div className="projects_buttons">
                                {item.site && <a href={item.site} className='projects_card_button'>Site <span className="purple">.open()</span></a>} 
                                {item.repository && <a href={item.repository} className='projects_card_button'>file <span className="purple">.repo()</span></a>} 
                            </div>
                        </div>
                    </div>
                <br/>
                <br/>
              </div>
          )
      })

      if(loader) CARD_DATA=<Skeleton/>

    return (
        <div className='projects'>
           <div className="flex">
                <h1 className="projects_head">Projects</h1>
                <img width='30px' src={Totoro} alt=""/>
           </div>

            {CARD_DATA}
        </div>
    )
}

export default Projects
