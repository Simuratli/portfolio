import * as actions from './actions'

const initialState = {
    sleep:false
}

const reducer = (state=initialState,action) =>{
    switch (action.type) {
        case actions.OPEN_SLEEP:
            return{
                sleep:!state.sleep
            }
        default:
            return state;
    }
}

export default reducer