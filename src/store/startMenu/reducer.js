import * as actionTypes from './actions'

const initialState = {
     startMenu:false
}

const reducer = (state=initialState,action) =>{ 
    switch (action.type) {
        case actionTypes.OPEN_MENU:
            return{
                startMenu:!state.startMenu
            }
        default:
            return state;
    }
} 

export default reducer