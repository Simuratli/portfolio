import * as actionTypes from './actions'

const initialState = {
    image:'https://i.pinimg.com/originals/b2/2a/a2/b22aa22b2f3f55b6468361158d52e2e7.gif'
}

const reducer = (state=initialState,action) =>{
    switch (action.type) {
        case actionTypes.CHANGE_WALLPAPER:
            localStorage.setItem('img',action.image)
            return{
                image:action.image
            }
        default:
            return state;
    }
}

export default reducer