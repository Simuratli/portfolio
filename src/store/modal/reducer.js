import * as actionTypes from './actions'

const initialState = {
    modal:true,
    opener:null
}

const reducer = (state=initialState,action) => {
    switch (action.type) {
        case actionTypes.OPEN_MODAL:
            return{
                ...state,
                modal:!state.modal
            }
        case actionTypes.OPEN_ITEM:
            return {
                ...state,
                opener:action.name
            }    
        default:
            return state;
    }
}

export default reducer