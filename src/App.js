import './App.css';
import Wall from './components/Wall/Wall'
import Sidebar from './components/Sidebar/Sidebar'
import StartMenu from './components/StartMenu/StartMenu'
import Modal from './components/Modal/Modal'
import React  from 'react'
import {connect} from 'react-redux'
import About from './components/About/About'
import Projects from './components/Projects/Projects'
import Music from './components/Music/Music'
import Sleep from './components/Sleep/Sleep'
import Wallpaper from './components/Wallpaper/Wallpaper'
import Doodle from './assets/doodle.gif'




function App(props) {
  
  let modal = props.openerText === 'Profile' ? <div>Profile</div>: 
              props.openerText === 'Projects' ? <Projects/>:
              props.openerText === 'About' ? <About/>:
              props.openerText === 'Music' ? <Music/>:
              props.openerText === 'Wallpaper' ? <Wallpaper/>:
              props.openerText === 'Internet' ? <img width="100%" alt='Doodle' src={Doodle} />:
              null

  return (
      <div className="App">
          <Sleep/>
        
          <Wall>
            <Sidebar/>
            <Modal title={props.openerText}>
                {modal}
            </Modal>
            <StartMenu/>
          </Wall>
      </div>
  );
}


const mapStateToProps = state => {
    return {
        openerText:state.modalReducer.opener
    }
}

export default connect(mapStateToProps)(App);
