import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from 'react-redux'
import {createStore,applyMiddleware, compose,combineReducers } from 'redux'
import startMenuReducer from './store/startMenu/reducer'
import modalReducer from './store/modal/reducer'
import sleepReducer from './store/sleep/reducer'
import wallReducer from './store/wallpaper/reducer'
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  startMenuReducer,
  modalReducer,
  sleepReducer,
  wallReducer
})
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer,composeEnhancers(applyMiddleware(thunk)))


ReactDOM.render(
  <Provider store={store}>
  <React.StrictMode>
    <App />
  </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
